﻿using Monopoly_An_1.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopoly_An_1
{
     partial class GameForm : Form
    {
        public MonopolyGame game = new MonopolyGame();
        
        public GameForm()
        {
            InitializeComponent();
        }

      public GameForm(List <Player> playerList)
        {
            
            InitializeComponent();
          game.PlayerList = playerList;
        }


        private void rollButton_Click(object sender, EventArgs e)
        {

        }

        private void GameForm_Load(object sender, EventArgs e)
        {

            playersGrid.DataSource = game.PlayerList;
            cellGrid.DataSource = game.Table;
        }
    }
}
