﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopoly_An_1.Entities
{
     class MonopolyGame
    {
        public List<Player> playerList = new List<Player>();

        public List<Player> PlayerList
        {
            get { return playerList; }
            set { playerList = value; }
        }
        List<Cell> table = new List<Cell>();

        public List<Cell> Table
        {
            get { return table; }
            set { table = value; }
        }

        Dice dice = new Dice();

        public Dice Dice
        {
            get { return dice; }
            set { dice = value; }
        }


    }
}
