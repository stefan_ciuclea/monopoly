﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopoly_An_1.Entities
{
    class Cell
    {
        string name;

        public int Y
        {
            get
            {
                return this.y;
            }
            set
            {
                this.y = value;
            }
        }

        public int X
        {
            get
            {
                return this.x;
            }
            set
            {
                this.x = value;
            }
        }

        public PlayerTypes Owner
        {
            get
            {
                return this.owner;
            }
            set
            {
                this.owner = value;
            }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        int price;

        public int Price
        {
            get { return price; }
            set { price = value; }
        }
        int rent;

        public int Rent
        {
            get { return rent; }
            set { rent = value; }
        }
        PlayerTypes owner;
        int x, y;
    }
}
