Actors
- Player


As a Player I can
UC1 - Edit my data
UC2 - join a new game
UC3 - roll a dice
UC4 - buy an available property
UC5 - pay the toll
UC6 - lose the game
UC7 - get money from the bank
UC8 - trade with another player
UC9 - win the game

HOMEWORK?
- define the entities use in app
- tutorial on how to use user controls 
- implement UC1 - UC9
